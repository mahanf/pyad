#from pyad import aduser
#from pyad import adcontainer
#from pyad import adcomputer
import pyad

#Settings für Serverzugang
#pyad.set_defaults(ldap_server="192.168.56.10", username="Administrator", password="BS14Lf3")

#Ein USer-Objekt anzeigen lassen
user = pyad.aduser.ADUser.from_dn("cn=Norbert Nagel, ou=Ben_IT, ou=Benutzer, ou=GEEK-Fitness, dc=geek, dc=local")
print(user)

#Überblick über Attribute eines Users
print(user.get_allowed_attributes())

#Konkretes Attribut ändern
#print(user.get_attribute('homephone'))
#print(user.update_attribute('homephone','123456'))
#print(user.get_attribute('homephone'))

#Ein neues Computer-Objekt erzeugen
ou = pyad.adcontainer.ADContainer.from_dn("ou=Cmp_IT, ou=Computer, ou=GEEK-Fitness, dc=geek, dc=local")
new_computer = pyad.adcomputer.ADComputer.create("PC_Nagel", ou, optional_attributes=dict(description = "von Python angelegt...",dnshostname = "12.34.56.78"))

#Weiss ich nicht mehr was ich damit meine...
#Passt nicht die Attribute nur den Anzeigenamen an
#Inkonsistenz im Objekt...
#user.rename("Norbert Nogel")

print(user)